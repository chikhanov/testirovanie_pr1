/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
interface Calc {
    int getSum(int x, int y);

    int getDivide(int x, int y);
}
