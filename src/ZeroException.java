/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
class ZeroException extends RuntimeException {

    ZeroException() {}

    ZeroException(String name) {
        super(name + " could not be 0 !");
    }
}
