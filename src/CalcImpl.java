/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
class CalcImpl implements Calc {

    public int getSum(int x, int y) {
        return x + y;
    }

    public int getDivide(int x, int y) {
        if (y == 0) {
            throw new ZeroException("The denominator");
        }
        return x / y;
    }
}
