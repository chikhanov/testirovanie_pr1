/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static org.junit.Assert.assertEquals;

public class CalcTest {
    @Rule
    public TestRule watcher = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("Starting test: " + description.getMethodName());
        }
    };

    @Test
    public void getSumTest() {
        Calc c = new CalcImpl();
        assertEquals(c.getSum(20, 30), 50);
    }

    @Test
    public void getDividerSuccessTest() {
        // Check happy case
        Calc calc = new CalcImpl();
        assertEquals(calc.getDivide(20, 5), 4);
    }

    @Rule
    public ExpectedException expectedZeroEx = ExpectedException.none();

    @Test
    public void getDividerExceptionTest() {
        // Check 0 is handling in method
        expectedZeroEx.expect(ZeroException.class);
        expectedZeroEx.expectMessage("The denominator could not be 0 !");
        Calc calc = new CalcImpl();
        calc.getDivide(5, 0);
    }
}
